namespace mock_dependency_inversion
{
    public class Order
    {

        private String _product;

        public Order(String product)
        {
		_product = product;
        }

        public boolean IsValid()
        {
		return _product == "";
        }
        
    }
}
