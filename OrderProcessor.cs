﻿using mock_dependency_inversion.Dependencies;

namespace mock_dependency_inversion
{
    public class OrderProcessor
    {
        public void Process(Order order)
        {
		    if (order.IsValid() && new DBRepository().Save(order))
		    {
			    new MailSender().SendConfirmationMessage(order);
		    }
        }
    }
}
