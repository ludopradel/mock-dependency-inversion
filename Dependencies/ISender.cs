namespace mock_dependency_inversion
{
    public interface ISender
    {
        public SendConfirmationMessage(Order order);
    }
}
