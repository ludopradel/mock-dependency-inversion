namespace mock_dependency_inversion
{
    public class DBRepository : IRepository
    {
        public Save(Order order)
        {
            throw new InvalidOperationException("Don't call me directly, use a double instead !");
        }
    }
}
