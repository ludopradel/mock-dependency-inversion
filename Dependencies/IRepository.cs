namespace mock_dependency_inversion
{
    public interface IRepository
    {
        public Save(Order order);
    }
}